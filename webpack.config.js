import path                         from 'node:path';
import {fileURLToPath}              from 'node:url';

import ReactRefreshWebpackPlugin    from '@pmmmwh/react-refresh-webpack-plugin';

import autoprefixer                 from 'autoprefixer';
import cssnano                      from 'cssnano';
import HtmlWebpackPlugin            from 'html-webpack-plugin';
import MiniCssExtractPlugin         from 'mini-css-extract-plugin';
import tailwindcss                  from 'tailwindcss';
import webpack                      from 'webpack';

import tailwindConfig               from './tailwind.config.js';


const publicDirectory = fileURLToPath(new URL('public', import.meta.url));
const sourceDirectory = fileURLToPath(new URL('src', import.meta.url));

const robotoFontPath = fileURLToPath(path.dirname(import.meta.resolve('@fontsource/roboto/package.json')));
const tailwindcssPath = fileURLToPath(path.dirname(import.meta.resolve('tailwindcss/package.json')));

export default {
    devtool: process.env.NODE_ENV === 'production' ? undefined : 'inline-source-map',

    devServer: {
        client: {
            overlay: false
        },
        historyApiFallback: true,
        hot: true,
        liveReload: false,
        static: [
            {directory: publicDirectory},
            {directory: '/root/eat-for-week-images', publicPath: '/images'}
        ]
    },

    entry: `${sourceDirectory}/main.jsx`,

    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',

    module: {
        rules: [
            {
                test: /\.jsx?$/u,
                include: sourceDirectory,
                loader: 'babel-loader',
                resolve: {
                    fullySpecified: false
                }
            },

            {
                test: /\.(?:woff|woff2)$/u,
                type: 'asset/resource',
                generator: {
                    filename: 'fonts/[hash][ext][query]'
                }
            },

            {
                test: /\.css$/u,
                include: [robotoFontPath],
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    // Minimize font files (using cssnano via postcss) only for production builds
                    ...(process.env.NODE_ENV === 'production' ? [
                        {
                            loader: 'postcss-loader',
                            options: {
                                postcssOptions: {
                                    plugins: [cssnano({preset: 'default'})]
                                }
                            }
                        }
                    ] : [])
                ]
            },

            {
                test: /\.css$/u,
                include: [tailwindcssPath],
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            // Defining postcss config here instead of postcss.config.js is a workaround for ESM currently
                            postcssOptions: {
                                plugins: [
                                    autoprefixer(),
                                    tailwindcss(tailwindConfig), // Passing the config directly is a workaround for ESM currently
                                    ...(process.env.NODE_ENV === 'production' ? [cssnano({preset: 'default'})] : [])
                                ]
                            }
                        }
                    }
                ]
            }
        ]
    },

    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/u,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },

    output: {
        filename: process.env.NODE_ENV === 'production' ? 'js/[name].[contenthash].js' : 'js/[name].js',
        path: publicDirectory,
        publicPath: '/'
    },

    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            hash: process.env.NODE_ENV === 'production',
            template: fileURLToPath(new URL('index.html', import.meta.url))
        }),

        new MiniCssExtractPlugin({
            filename: process.env.NODE_ENV === 'production' ? 'css/[name].[contenthash].css' : 'css/[name].css'
        }),

        new webpack.DefinePlugin({
            API_DOMAIN: JSON.stringify(process.env.NODE_ENV === 'production' ? 'https://eatforweek.duckdns.org' : 'http://127.0.0.1:3000'),
            API_GATEWAY_PATH: JSON.stringify(process.env.NODE_ENV === 'production' ? '/api' : false),
            API_URL: JSON.stringify(process.env.NODE_ENV === 'production' ? 'https://eatforweek.duckdns.org/api' : 'http://127.0.0.1:3000'),
            UI_URL: JSON.stringify(process.env.NODE_ENV === 'production' ? 'https://eatforweek.duckdns.org' : 'http://127.0.0.1:8080')
        }),

        ...(process.env.NODE_ENV === 'production'
            ? []
            : [new ReactRefreshWebpackPlugin({include: sourceDirectory})]
        )
    ],

    resolve: {
        extensions: ['.js', '.jsx']
    },

    stats: process.env.NODE_ENV === 'production' ? undefined : 'minimal'
};
