# Eat for Week UI

## Development

### Prerequisites
- [Node.js](https://nodejs.org/en)
- [pnpm](https://pnpm.io/)

### Setup
```console
pnpm install
```

### Run Locally
```console
pnpm dev
```
