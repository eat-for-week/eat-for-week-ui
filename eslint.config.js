import babelParser      from '@babel/eslint-parser';

import jsConfig         from '@eslint/js';

import stylisticPlugin  from '@stylistic/eslint-plugin';

import reactPlugin      from 'eslint-plugin-react';
import reactAllConfig   from 'eslint-plugin-react/configs/all.js';
import unicornPlugin    from 'eslint-plugin-unicorn';
import globals          from 'globals';


const sharedRules = {
    ...jsConfig.configs.all.rules,
    ...stylisticPlugin.configs['all-flat'].rules,
    ...unicornPlugin.configs.all.rules,

    '@stylistic/array-element-newline': ['error', 'consistent'],
    '@stylistic/arrow-parens': ['error', 'as-needed'],
    '@stylistic/dot-location': ['error', 'property'],
    '@stylistic/function-call-argument-newline': ['error', 'consistent'],
    '@stylistic/function-paren-newline': 'off',
    '@stylistic/key-spacing': 'off',
    '@stylistic/multiline-ternary': 'off',
    '@stylistic/newline-per-chained-call': 'off',
    '@stylistic/no-extra-parens': 'off',
    '@stylistic/no-multi-spaces': ['error', {exceptions: {VariableDeclarator: true, ImportDeclaration: true}}],
    '@stylistic/object-property-newline': ['error', {allowAllPropertiesOnSameLine: true}],
    '@stylistic/padded-blocks': 'off',
    '@stylistic/quotes': ['error', 'single'],
    '@stylistic/quote-props': ['error', 'as-needed'],

    camelcase: 'off',
    'capitalized-comments': 'off',
    'consistent-return': 'off',
    'id-length': 'off',
    'line-comment-position': 'off',
    'max-statements': 'off',
    'max-lines-per-function': 'off',
    'multiline-comment-style': 'off',
    'new-cap': 'off',
    'no-console': 'off',
    'no-inline-comments': 'off',
    'no-magic-numbers': 'off',
    'no-nested-ternary': 'off',
    'no-plusplus': 'off',
    'no-ternary': 'off',
    'no-undefined': 'off',
    'one-var': 'off',
    'sort-imports': 'off',
    'sort-keys': 'off',

    // Reports false positives - Refer to https://github.com/sindresorhus/eslint-plugin-unicorn/issues/1193
    'unicorn/no-array-callback-reference': 'off',

    // Reports false positives - Refer to https://github.com/sindresorhus/eslint-plugin-unicorn/issues/1394
    'unicorn/no-array-method-this-argument': 'off',

    'unicorn/consistent-function-scoping': 'off',
    'unicorn/no-array-reduce': 'off',
    'unicorn/no-nested-ternary': 'off',
    'unicorn/no-null': 'off'
};

const appRules = {
    ...reactAllConfig.rules,

    'react/forbid-component-props': 'off',
    'react/forbid-prop-types': 'off',
    'react/function-component-definition': ['error', {namedComponents: 'arrow-function', unnamedComponents: 'arrow-function'}],
    'react/jsx-handler-names': 'off',
    'react/jsx-max-depth': 'off',
    'react/jsx-max-props-per-line': 'off',
    'react/jsx-newline': 'off',
    'react/jsx-no-literals': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'react/jsx-props-no-multi-spaces': 'off',
    'react/jsx-sort-props': ['error', {callbacksLast: true, reservedFirst: true}],
    'react/react-in-jsx-scope': 'off',
    'react/sort-prop-types': ['off'],

    'unicorn/filename-case': ['error', {case: 'pascalCase', ignore: ['main.jsx']}],
    'unicorn/prevent-abbreviations': ['error', {allowList: {mapStateToProps: true, mapDispatchToProps: true}}]
};

const projectRules = {
    // 'unicorn/prefer-module': 'off'
};

export default [
    {
        files: ['src/**/*.+(js|jsx)'],

        languageOptions: {
            globals: {...globals.browser, API_DOMAIN: true, API_GATEWAY_PATH: true, API_URL: true, UI_URL: true},
            parser: babelParser,
            parserOptions: {
                ecmaFeatures: {
                    jsx: true
                }
            }
        },

        linterOptions: {
            noInlineConfig: true
        },

        plugins: {
            '@stylistic': stylisticPlugin,
            react: reactPlugin,
            unicorn: unicornPlugin
        },

        rules: {...sharedRules, ...appRules},

        settings: {
            react: {
                version: 'detect'
            }
        }
    },

    {
        files: ['*.js'],

        languageOptions: {
            globals: globals.node
        },

        linterOptions: {
            noInlineConfig: true
        },

        plugins: {
            '@stylistic': stylisticPlugin,
            unicorn: unicornPlugin
        },

        rules: {...sharedRules, ...projectRules}
    }
];
