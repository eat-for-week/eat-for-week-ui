import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import 'tailwindcss/tailwind.css';

import {green, orange}                      from '@mui/material/colors';
import {ThemeProvider, createTheme}         from '@mui/material/styles';
import CssBaseline                          from '@mui/material/CssBaseline';

import {configureStore}                     from '@reduxjs/toolkit';

import React                                from 'react';
import ReactDOM                             from 'react-dom/client';
import {Provider}                           from 'react-redux';
import SuperTokens, {SuperTokensWrapper}    from 'supertokens-auth-react';
import EmailPassword                        from 'supertokens-auth-react/recipe/emailpassword';
import Session                              from 'supertokens-auth-react/recipe/session';

import App                                  from './App.jsx';
import reducer                              from './reducers';


SuperTokens.init({
    appInfo: {
        appName: 'Eat for Week',
        apiDomain: API_DOMAIN,
        websiteDomain: UI_URL,
        apiGatewayPath: API_GATEWAY_PATH || undefined
    },
    recipeList: [
        EmailPassword.init(),
        Session.init()
    ]
});

const store = configureStore({
    reducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        // Significant slowdown when left enabled
        immutableCheck: false
    })
});
const theme = createTheme({palette: {primary: {main: green[600]}, secondary: {main: orange[800]}}});

ReactDOM.createRoot(document.querySelector('#root')).render(
    <React.StrictMode>
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <CssBaseline />

                <SuperTokensWrapper>
                    <App />
                </SuperTokensWrapper>
            </ThemeProvider>
        </Provider>
    </React.StrictMode>
);
