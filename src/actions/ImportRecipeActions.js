import {createAction}   from '@reduxjs/toolkit';

import {UpdateRecipes}  from './RecipesActions';


export const UpdateImportRecipe = createAction('UPDATE_IMPORT_RECIPE');


export const addNewRecipe = () => (dispatch, getState) => {
    dispatch(UpdateImportRecipe({addRecipeRequest: {pending: true}}));
    fetch(`${API_URL}/recipe`, {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(getState().ImportRecipe.getNewRecipeRequest.data)})
        .then(response => response.json().then(data => {
            if (!response.ok) {
                return dispatch(UpdateImportRecipe({addRecipeRequest: {error: data}}));
            }

            dispatch(UpdateImportRecipe({addRecipeRequest: {data}, modalOpen: false}));
            const updatedUserRecipes = [data, ...(getState().Recipes.getUserRecipesRequest?.data || [])];
            dispatch(UpdateRecipes({getUserRecipesRequest: {data: updatedUserRecipes}}));
        }))
        .catch(error => dispatch(UpdateImportRecipe({addRecipeRequest: {error}})));
};

export const getNewRecipe = () => (dispatch, getState) => {
    if (!getState().ImportRecipe.urlInputValue) {
        return;
    }

    dispatch(UpdateImportRecipe({getNewRecipeRequest: {pending: true}}));
    fetch(`${API_URL}/recipe/${encodeURIComponent(getState().ImportRecipe.urlInputValue)}`)
        .then(response => response.json().then(data => dispatch(UpdateImportRecipe({
            getNewRecipeRequest: response.ok ? {data} : {error: data}
        }))))
        .catch(error => dispatch(UpdateImportRecipe({getNewRecipeRequest: {error}})));
};
