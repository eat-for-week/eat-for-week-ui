import {createAction} from '@reduxjs/toolkit';


export const UpdateRecipes = createAction('UPDATE_RECIPES');


export const fetchUserRecipes = () => dispatch => {
    dispatch(UpdateRecipes({getUserRecipesRequest: {pending: true}}));
    fetch(`${API_URL}/recipes`)
        .then(response => response.json())
        .then(data => dispatch(UpdateRecipes({getUserRecipesRequest: {data}})))
        .catch(error => dispatch(UpdateRecipes({getUserRecipesRequest: {error}})));
};
