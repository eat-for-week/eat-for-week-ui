import {createAction}   from '@reduxjs/toolkit';

import {UpdateRecipes}  from './RecipesActions';


export const UpdateLinking = createAction('UPDATE_LINKING');


export const addUserLink = () => (dispatch, getState) => {
    const emailInputValueTrimmed = getState().Linking.emailInputValue.trim();
    if (!emailInputValueTrimmed) {
        return;
    }
    if (getState().Linking.addUserLinkRequest?.pending) {
        return;
    }
    if (getState().Linking.fetchUserLinkingRequest.data.some(({email}) => email === emailInputValueTrimmed)) {
        return;
    }

    dispatch(UpdateLinking({addUserLinkRequest: {pending: true}}));
    fetch(`${API_URL}/linking/${encodeURIComponent(emailInputValueTrimmed)}`, {method: 'POST'})
        .then(response => response.json().then(data => {
            if (!response.ok) {
                return dispatch(UpdateLinking({addUserLinkRequest: {error: response.status === 404 ? {userNotFound: true} : data}}));
            }

            dispatch(UpdateLinking({addUserLinkRequest: null, emailInputValue: '', fetchUserLinkingRequest: {data: data.user_linking}}));
            dispatch(UpdateRecipes({getUserRecipesRequest: {data: data.recipes}}));
        }))
        .catch(error => dispatch(UpdateLinking({addUserLinkRequest: {error}})));
};

export const deleteUserLink = id => dispatch => {
    dispatch(UpdateLinking({deleteUserLink: {id, request: {pending: true}}}));
    fetch(`${API_URL}/linking/${id}`, {method: 'DELETE'})
        .then(response => response.json().then(data => {
            if (!response.ok) {
                return dispatch(UpdateLinking({deleteUserLink: {id, request: {error: data}}}));
            }

            dispatch(UpdateLinking({deleteUserLink: null, fetchUserLinkingRequest: {data: data.user_linking}}));
            dispatch(UpdateRecipes({getUserRecipesRequest: {data: data.recipes}}));
        }))
        .catch(error => dispatch(UpdateLinking({deleteUserLink: {id, request: {error}}})));
};

export const fetchUserLinking = () => (dispatch, getState) => {
    if (getState().Linking.fetchUserLinkingRequest?.pending || getState().Linking.fetchUserLinkingRequest?.data) {
        return;
    }

    dispatch(UpdateLinking({fetchUserLinkingRequest: {pending: true}}));
    fetch(`${API_URL}/linking`)
        .then(response => response.json().then(data => {
            if (!response.ok) {
                return dispatch(UpdateLinking({fetchUserLinkingRequest: {error: data}}));
            }

            dispatch(UpdateLinking({fetchUserLinkingRequest: {data}}));
        }))
        .catch(error => dispatch(UpdateLinking({fetchUserLinkingRequest: {error}})));
};
