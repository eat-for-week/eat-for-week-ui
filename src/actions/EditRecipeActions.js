import {createAction}                   from '@reduxjs/toolkit';

import {UpdateRecipes}                  from './RecipesActions';

import {getRecipePropertyValidToSave}   from '../selectors';


export const EditRecipeAdd = createAction('EDIT_RECIPE_ADD');
export const EditRecipeRemove = createAction('EDIT_RECIPE_REMOVE');
export const EditRecipeSet = createAction('EDIT_RECIPE_SET');
export const EditRecipeStart = createAction('EDIT_RECIPE_START');
export const UpdateEditRecipe = createAction('UPDATE_EDIT_RECIPE');


export const saveRecipeEdits = () => (dispatch, getState) => {
    dispatch(UpdateEditRecipe({saveEditsRequest: {pending: true}}));

    fetch(`${API_URL}/edit/${getState().EditRecipe.editingSingleRecipeId}`, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            ...getRecipePropertyValidToSave(getState(), 'ingredients') && {ingredients: getState().EditRecipe.ingredients.map(({text}) => text)},
            ...getRecipePropertyValidToSave(getState(), 'instructions_list') && {instructions_list: getState().EditRecipe.instructions_list.map(({text}) => text)}
        })
    }).then(response => response.json().then(data => {
        if (!response.ok) {
            return dispatch(UpdateEditRecipe({saveEditsRequest: {error: data}}));
        }

        dispatch(UpdateEditRecipe({saveEditsRequest: null, editingSingleRecipeId: null, ingredients: null, instructions_list: null}));
        const updatedUserRecipes = getState().Recipes.getUserRecipesRequest.data.reduce((r, recipe) => {
            r.push(recipe.id === data.id ? data : recipe);
            return r;
        }, []);
        dispatch(UpdateRecipes({getUserRecipesRequest: {data: updatedUserRecipes}}));
    })).catch(
        error => dispatch(UpdateEditRecipe({saveEditsRequest: {error}}))
    );
};

export const toggleEditingRecipe = recipeId => (dispatch, getState) => dispatch(getState().EditRecipe.editingSingleRecipeId
    ? UpdateEditRecipe({editingSingleRecipeId: null, ingredients: null, instructions_list: null})
    : EditRecipeStart({recipe: getState().Recipes.getUserRecipesRequest.data.find(({id}) => id === recipeId)})
);
