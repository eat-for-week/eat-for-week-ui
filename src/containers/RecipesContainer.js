import {connect}                                        from 'react-redux';
import RecipesComponent                                 from '../components/RecipesComponent';

import {addNewRecipe, getNewRecipe, UpdateImportRecipe} from '../actions/ImportRecipeActions';
import {fetchUserRecipes}                               from '../actions/RecipesActions';


const mapStateToProps = (state, ownProps) => ({
    ImportRecipe: state.ImportRecipe,
    Recipes: state.Recipes,
    userId: ownProps.userId
});

const mapDispatchToProps = dispatch => ({
    addNewRecipe: () => dispatch(addNewRecipe()),

    fetchUserRecipes: () => dispatch(fetchUserRecipes()),

    goBackToUrlInput: () => dispatch(UpdateImportRecipe({addRecipeRequest: null, getNewRecipeRequest: null})),

    setModalOpenState: event => dispatch(UpdateImportRecipe(event.currentTarget.dataset.open === 'true' ? {
        addRecipeRequest: null, getNewRecipeRequest: null, modalOpen: true, urlInputValue: ''
    } : {
        modalOpen: false
    })),

    setUrlInputValue: event => dispatch(UpdateImportRecipe({addRecipeRequest: null, getNewRecipeRequest: null, urlInputValue: event.target.value})),

    submitLookupRecipe: event => {
        event.preventDefault();
        dispatch(getNewRecipe());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(RecipesComponent);
