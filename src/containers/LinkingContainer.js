import {connect}                                                        from 'react-redux';
import LinkingComponent                                                 from '../components/LinkingComponent';

import {addUserLink, deleteUserLink, fetchUserLinking, UpdateLinking}   from '../actions/LinkingActions';


const mapStateToProps = state => ({
    Linking: state.Linking
});

const mapDispatchToProps = dispatch => ({
    addUserLink:       event => {
        event.preventDefault();
        dispatch(addUserLink());
    },

    deleteUserLink:    event => dispatch(deleteUserLink(event.currentTarget.dataset.id)),

    fetchUserLinking:   () => dispatch(fetchUserLinking()),

    setEmailInputValue: event => dispatch(UpdateLinking({addUserLinkRequest: null, emailInputValue: event.target.value})),

    setShowingDialog:   event => {
        dispatch(fetchUserLinking());
        dispatch(UpdateLinking({
            addUserLinkRequest: null,
            deleteUserLink: null,
            showingDialog: event.currentTarget.dataset.showing === 'true'
        }));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LinkingComponent);
