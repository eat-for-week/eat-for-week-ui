import {connect}                        from 'react-redux';
import RecipeComponent                  from '../components/RecipeComponent';

import {
    EditRecipeAdd,
    EditRecipeRemove,
    EditRecipeSet,
    UpdateEditRecipe,
    saveRecipeEdits,
    toggleEditingRecipe
} from '../actions/EditRecipeActions';
import {
    fetchUserRecipes
} from '../actions/RecipesActions';

import {getRecipePropertyValidToSave} from '../selectors';


const mapStateToProps = state => ({
    EditRecipe: state.EditRecipe,
    Recipes: state.Recipes,
    saveValid: getRecipePropertyValidToSave(state, 'ingredients') || getRecipePropertyValidToSave(state, 'instructions_list')
});

const mapDispatchToProps = dispatch => ({
    clearEditSaveRequest: () => dispatch(UpdateEditRecipe({saveEditsRequest: null})),

    editAdd: event => dispatch(EditRecipeAdd({collectionName: event.currentTarget.dataset.collectionName})),

    editDelete: event => dispatch(EditRecipeRemove({collectionName: event.currentTarget.dataset.collectionName, localId: Number(event.currentTarget.dataset.localId)})),

    editSet: event => dispatch(EditRecipeSet({collectionName: event.currentTarget.dataset.collectionName, localId: Number(event.currentTarget.dataset.localId), value: event.target.value})),

    fetchUserRecipes: () => dispatch(fetchUserRecipes()),

    saveEdits: () => dispatch(saveRecipeEdits()),

    setEditingRecipe: event => dispatch(toggleEditingRecipe(event.currentTarget.dataset.recipeId))
});

export default connect(mapStateToProps, mapDispatchToProps)(RecipeComponent);
