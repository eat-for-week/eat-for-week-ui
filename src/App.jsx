import DateRangeOutlinedIcon                                                    from '@mui/icons-material/DateRangeOutlined';
import DinnerDiningOutlinedIcon                                                 from '@mui/icons-material/DinnerDiningOutlined';
import LocalGroceryStoreIcon                                                    from '@mui/icons-material/LocalGroceryStore';
import SummarizeOutlinedIcon                                                    from '@mui/icons-material/SummarizeOutlined';
import {AppBar, BottomNavigation, BottomNavigationAction, Toolbar, Typography}  from '@mui/material';

import {EmailPasswordPreBuiltUI}                                                from 'supertokens-auth-react/recipe/emailpassword/prebuiltui';
import {useSessionContext}                                                      from 'supertokens-auth-react/recipe/session';
import {canHandleRoute, getRoutingComponent}                                    from 'supertokens-auth-react/ui';

import {useLocation, Link, Route, Switch}                                       from 'wouter';

import LinkingContainer                                                         from './containers/LinkingContainer';
import RecipeContainer                                                          from './containers/RecipeContainer';
import RecipesContainer                                                         from './containers/RecipesContainer';


const recipesRoute = {icon: <SummarizeOutlinedIcon />, label: 'Recipes', to: '/recipes'};
const planRoute = {icon: <DateRangeOutlinedIcon />, label: 'Plan', to: '/plan'};
const groceryRoute = {icon: <LocalGroceryStoreIcon />, label: 'Grocery', to: '/grocery'};
const routes = [recipesRoute, planRoute, groceryRoute];

const App = () => {
    const session = useSessionContext();
    const [location] = useLocation();

    // Since the component from getRoutingComponent will only render at /auth, need to use canHandleRoute
    if (canHandleRoute([EmailPasswordPreBuiltUI])) {
        return getRoutingComponent([EmailPasswordPreBuiltUI]);
    }

    if (session.loading) {
        return null;
    }
    if (!session.doesSessionExist) {
        window.location.replace('/auth');
        return null;
    }

    const {bottomNavigationLinks, bottomNavigationValue} = routes.reduce((r, {icon, label, to}, index) => {
        if (location.startsWith(to)) {
            r.bottomNavigationValue = index;
        }

        r.bottomNavigationLinks.push(
            <BottomNavigationAction key={to} classes={{label: 'font-bold text-lg leading-none'}} component={Link} icon={icon} label={label} to={to} />
        );

        return r;
    }, {bottomNavigationLinks: [], bottomNavigationValue: 0});

    return (
        <>
            <AppBar position="sticky">
                <Toolbar className="justify-between">
                    <div className="flex items-center">
                        <DinnerDiningOutlinedIcon className="text-3xl mr-2" />
                        <Typography variant="h6">Eat for Week</Typography>
                    </div>

                    <LinkingContainer />
                </Toolbar>
            </AppBar>

            <Switch>
                <Route path="/recipe/:recipeId">
                    {parameters => <RecipeContainer recipeId={parameters.recipeId} />}
                </Route>

                <Route path={planRoute.to}>
                    <div className="fixed w-screen h-screen flex flex-col items-center justify-center text-zinc-300 font-bold text-5xl -top-[32px]">
                        Plan
                    </div>
                </Route>
                <Route path={groceryRoute.to}>
                    <div className="fixed w-screen h-screen flex flex-col items-center justify-center text-zinc-300 font-bold text-5xl -top-[32px]">
                        Grocery
                    </div>
                </Route>
                <Route>
                    <RecipesContainer userId={session.userId} />
                </Route>
            </Switch>

            <BottomNavigation className="fixed bottom-0 w-full" showLabels value={bottomNavigationValue}>
                {bottomNavigationLinks}
            </BottomNavigation>
        </>
    );
};

export default App;
