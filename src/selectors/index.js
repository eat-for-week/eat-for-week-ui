import {createSelector} from '@reduxjs/toolkit';

const getEditRecipeProperty = (state, propertyName) => state.EditRecipe[propertyName];
const getSourceRecipeProperty = (state, propertyName) => state.Recipes?.getUserRecipesRequest?.data?.find(
    ({id}) => id === state.EditRecipe.editingSingleRecipeId
)?.[propertyName];

export const getRecipePropertyValidToSave = createSelector(
    [getEditRecipeProperty, getSourceRecipeProperty],
    (editRecipeProperty, sourceRecipeProperty) => {
        if (!(editRecipeProperty && sourceRecipeProperty)) {
            return false;
        }

        let anyPropertyDiffered = false;
        for (const [index, {text}] of editRecipeProperty.entries()) {
            if (!text.trim()) {
                return false;
            }
            if (text !== sourceRecipeProperty[index]) {
                anyPropertyDiffered = true;
            }
        }

        return editRecipeProperty.length !== sourceRecipeProperty.length || anyPropertyDiffered;
    }
);
