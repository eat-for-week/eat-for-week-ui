import {combineReducers}    from 'redux';

import EditRecipe           from './EditRecipe';
import ImportRecipe         from './ImportRecipe';
import Linking              from './Linking';
import Recipes              from './Recipes';


export default combineReducers({
    EditRecipe,
    ImportRecipe,
    Linking,
    Recipes
});
