import {createReducer} from '@reduxjs/toolkit';

import {UpdateLinking} from '../actions/LinkingActions';


const defaultState = {
    addUserLinkRequest: null,
    deleteUserLink: null,
    emailInputValue: '',
    fetchUserLinkingingRequest: null,
    showingDialog: false
};
export default createReducer(defaultState, builder => builder
    .addCase(UpdateLinking, (state, action) => {
        Object.assign(state, action.payload);
    })
);
