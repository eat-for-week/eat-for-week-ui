import {createReducer} from '@reduxjs/toolkit';

import {UpdateImportRecipe} from '../actions/ImportRecipeActions';


export default createReducer({addRecipeRequest: null, getNewRecipeRequest: null, modalOpen: false, urlInputValue: ''}, builder => builder
    .addCase(UpdateImportRecipe, (state, action) => {
        Object.assign(state, action.payload);
    })
);
