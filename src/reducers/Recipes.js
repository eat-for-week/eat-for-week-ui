import {createReducer} from '@reduxjs/toolkit';

import {UpdateRecipes} from '../actions/RecipesActions';


export default createReducer({getUserRecipesRequest: null}, builder => builder
    .addCase(UpdateRecipes, (state, action) => {
        Object.assign(state, action.payload);
    })
);
