import {createReducer} from '@reduxjs/toolkit';

import {
    EditRecipeAdd,
    EditRecipeRemove,
    EditRecipeSet,
    EditRecipeStart,
    UpdateEditRecipe
} from '../actions/EditRecipeActions';


const defaultState = {
    editingSingleRecipeId: null,
    ingredients: null,
    instructions_list: null,
    saveEditsRequest: null
};

let localId = 0;

export default createReducer(defaultState, builder => builder
    .addCase(EditRecipeAdd, (state, action) => {
        if (state[action.payload.collectionName].at(-1)?.text?.trim() !== '') {
            state[action.payload.collectionName].push({localId: ++localId, text: ''});
        }
    })

    .addCase(EditRecipeRemove, (state, action) => {
        const index = state[action.payload.collectionName].findIndex(entry => entry.localId === action.payload.localId);
        state[action.payload.collectionName].splice(index, 1);
    })

    .addCase(EditRecipeSet, (state, action) => {
        const index = state[action.payload.collectionName].findIndex(entry => entry.localId === action.payload.localId);
        state[action.payload.collectionName][index].text = action.payload.value;
    })

    .addCase(EditRecipeStart, (state, action) => {
        state.editingSingleRecipeId = action.payload.recipe.id;
        state.ingredients = action.payload.recipe.ingredients.map(text => ({localId: ++localId, text}));
        state.instructions_list = action.payload.recipe.instructions_list.map(text => ({localId: ++localId, text}));
    })

    .addCase(UpdateEditRecipe, (state, action) => {
        Object.assign(state, action.payload);
    })
);
