import PropTypes                                                            from 'prop-types';

import AddCircleIcon                                                        from '@mui/icons-material/AddCircle';
import AddCircleOutlineIcon                                                 from '@mui/icons-material/AddCircleOutline';
import CloseIcon                                                            from '@mui/icons-material/Close';
import DeleteIcon                                                           from '@mui/icons-material/Delete';
import DeleteOutlineIcon                                                    from '@mui/icons-material/DeleteOutline';
import GroupIcon                                                            from '@mui/icons-material/Group';
import InfoIcon                                                             from '@mui/icons-material/Info';
import PersonAddIcon                                                        from '@mui/icons-material/PersonAdd';
import SickOutlinedIcon                                                     from '@mui/icons-material/SickOutlined';

import {Dialog, Divider, IconButton, InputAdornment, TextField, Typography} from '@mui/material';


const LinkingComponent = ({Linking, addUserLink, deleteUserLink, setEmailInputValue, setShowingDialog}) => (
    <>
        <IconButton className="text-white -mr-3 ml-4" data-showing="true" size="large" onClick={setShowingDialog}>
            <GroupIcon className="text-4xl" />
        </IconButton>

        <Dialog classes={{paper: 'max-w-none m-0 md:m-8 w-11/12 md:w-5/12 lg:w-1/3'}} maxWidth={false} open={Linking.showingDialog}>
            <div className="flex items-start justify-between pl-2 pt-1">
                <div>
                    <div className="flex items-center">
                        <GroupIcon className="opacity-80 mr-2 text-2xl md:text-3xl" color="primary" />
                        <div className="font-medium text-gray-600 text-xl md:text-2xl">Account Linking</div>
                    </div>
                    <Typography className="text-gray-400 -mt-2" variant="subtitle1">
                        Link your recipes, plans, and grocery list with other users
                    </Typography>
                </div>

                <IconButton className="relative -top-1 ml-4 text-zinc-100 brightness-50 hover:brightness-0 focus:brightness-0" onClick={setShowingDialog}><CloseIcon /></IconButton>
            </div>

            <div className="px-2 py-4">
                {Linking.fetchUserLinkingRequest?.error ? (
                    <Typography className="font-bold space-y-4 flex flex-col items-center justify-center" color="error">
                        <SickOutlinedIcon className=" opacity-90 text-[10rem]" />
                        <div className="text-center">Some went wrong while loading link details, please try again later</div>
                    </Typography>
                ) : Linking.fetchUserLinkingRequest?.pending ? (
                    <>
                        <div className="rounded h-[3.125rem] w-full bg-zinc-300 animate-pulse mt-6" />
                        <Divider className="mt-6 mb-4" />
                        <div className="rounded h-14 w-full bg-zinc-300 animate-pulse" />
                    </>
                ) : Linking.fetchUserLinkingRequest?.data && (
                    <>
                        <div className={`font-bold ${Linking.fetchUserLinkingRequest.data.length === 0 ? 'invisible' : ''}`}>
                            Linked with {Linking.fetchUserLinkingRequest.data.length} user{Linking.fetchUserLinkingRequest.data.length !== 1 && 's'}:
                        </div>
                        {Linking.fetchUserLinkingRequest.data.length === 0 ? (
                            <div className="flex items-center border border-solid border-zinc-300 rounded bg-zinc-50 px-2 py-3 opacity-60">
                                <InfoIcon className="mr-1 opacity-40" /> You are not currently linked with any users
                            </div>
                        ) : (
                            <div className="shadow-sm border border-solid border-zinc-300 rounded bg-zinc-50 divide-x-0 divide-y divide-solid divide-zinc-300">
                                {Linking.fetchUserLinkingRequest.data.map(({id, email}) => (
                                    <div key={id} className="flex items-center justify-between px-2 py-1">
                                        <div>
                                            {email}
                                            {Linking.deleteUserLink?.request?.error !== undefined && Linking.deleteUserLink.id === id &&
                                            <Typography className="text-sm" color="error" variant="subtitle2">
                                                Something went wrong removing link, please try again later
                                            </Typography>}
                                        </div>

                                        {Linking.deleteUserLink ? (Linking.deleteUserLink.id === id && (
                                            <IconButton className="-mr-2 opacity-70" color="error">
                                                <DeleteIcon />
                                            </IconButton>
                                        )) : (
                                            <IconButton className="-mr-2 group opacity-70 hover:opacity-100 focus:opacity-100" color="error" data-id={id} onClick={deleteUserLink}>
                                                <DeleteOutlineIcon className="group-hover:hidden group-focus:hidden" />
                                                <DeleteIcon className="hidden group-hover:block group-focus:block" />
                                            </IconButton>
                                        )}
                                    </div>
                                ))}
                            </div>
                        )}

                        <Divider className="mt-6 mb-4" />

                        <form onSubmit={addUserLink}>
                            <TextField
                                InputProps={{
                                    endAdornment: Linking.emailInputValue.trim() && (
                                        <InputAdornment position="end">
                                            <IconButton
                                                className="group"
                                                color="primary"
                                                disabled={Linking.fetchUserLinkingRequest.data.some(({email}) => email === Linking.emailInputValue.trim())}
                                                type="submit"
                                            >
                                                <AddCircleOutlineIcon className="group-hover:hidden" />
                                                <AddCircleIcon className="hidden group-hover:block" />
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                                autoFocus
                                className="w-full"
                                label={<div className="flex items-center"><PersonAddIcon className="mr-1" />Link via Email</div>}
                                value={Linking.emailInputValue}

                                onChange={setEmailInputValue}
                            />
                        </form>

                        {Linking.addUserLinkRequest?.error !== undefined && (
                            <Typography className="border border-solid border-current rounded bg-red-50 px-2 py-1 mt-2" color="error">
                                {Linking.addUserLinkRequest.error.userNotFound ? (
                                    'Eat for Week user was not found with that email'
                                ) : (
                                    'Something went wrong trying to link, please try again later'
                                )}
                            </Typography>
                        )}
                    </>
                )}
            </div>
        </Dialog>
    </>
);

const RequestShape = PropTypes.shape({data: PropTypes.any, error: PropTypes.object, pending: PropTypes.bool});
LinkingComponent.propTypes = {
    Linking:            PropTypes.shape({
        addUserLinkRequest: RequestShape,
        deleteUserLink: PropTypes.shape({id: PropTypes.string.isRequired, request: RequestShape.isRequired}),
        emailInputValue: PropTypes.string.isRequired,
        fetchUserLinkingRequest: RequestShape,
        showingDialog: PropTypes.bool
    }).isRequired,

    addUserLink:       PropTypes.func.isRequired,
    deleteUserLink:    PropTypes.func.isRequired,
    setEmailInputValue: PropTypes.func.isRequired,
    setShowingDialog:   PropTypes.func.isRequired
};

export default LinkingComponent;
