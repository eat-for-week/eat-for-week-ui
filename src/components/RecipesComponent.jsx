import PropTypes from 'prop-types';
import {useEffect} from 'react';

import AccessTimeIcon from '@mui/icons-material/AccessTime';
import AddIcon from '@mui/icons-material/Add';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import KitchenIcon from '@mui/icons-material/Kitchen';
import ListAltIcon from '@mui/icons-material/ListAlt';
import PersonIcon from '@mui/icons-material/Person';
import SickOutlinedIcon from '@mui/icons-material/SickOutlined';

import {Link} from 'wouter';

import {Button, CircularProgress, Dialog, Fab, IconButton, InputAdornment, Paper, TextField, Typography} from '@mui/material';

const recipeBoxShadow = 'shadow-[0px_3px_3px_-2px_rgba(0,0,0,0.2),0px_3px_4px_0px_rgba(0,0,0,0.14),0px_1px_8px_0px_rgba(0,0,0,0.12)]';
const recipeBoxShadowHover = 'hover:shadow-[0px_4px_5px_-2px_rgba(0,0,0,0.2),0px_7px_10px_1px_rgba(0,0,0,0.14),0px_2px_16px_1px_rgba(0,0,0,0.12)]';

const RecipesComponent = ({
    ImportRecipe, Recipes, userId,

    addNewRecipe, fetchUserRecipes, goBackToUrlInput, setModalOpenState, setUrlInputValue, submitLookupRecipe
}) => {
    useEffect(() => {
        if (!Recipes.getUserRecipesRequest?.data) {
            fetchUserRecipes();
        }
    }, []);

    const AddFabs = (
        <>
            <Fab aria-label="add" className="fixed bottom-20 right-12 inline-flex md:hidden" color="secondary" data-open="true" onClick={setModalOpenState}>
                <AddIcon className="text-2xl" />
            </Fab>
            <Fab aria-label="add" className="fixed bottom-20 right-16 md:right-6 lg:right-20 hidden md:inline-flex text-xl" color="secondary" data-open="true" variant="extended" onClick={setModalOpenState}>
                <AddIcon className="text-3xl mr-1" />
                Add
            </Fab>
        </>
    );

    return (
        <>
            {Recipes.getUserRecipesRequest?.error ? (
                <Typography className="container mx-auto px-8 min-h-[calc(100vh-64px)] flex flex-col items-center justify-center font-bold space-y-4" color="error">
                    <SickOutlinedIcon className=" opacity-90 text-[10rem]" />
                    <div className="text-5xl text-center leading-none">Oh no!</div>
                    <div className="text-center">There seems to be a problem, please try again later</div>
                </Typography>
            ) : Recipes.getUserRecipesRequest?.data?.length > 0 ? (
                <div className="container mx-auto px-4 md:px-12 lg:px-32 xl:px-48 pt-4">
                    {Recipes.getUserRecipesRequest.data.map(recipe => (
                        <Paper
                            key={recipe.id}
                            className={`block w-full min-h-[80px] md:min-h-[128px] rounded my-4 pr-2 no-underline duration-100 ${recipeBoxShadow} ${recipeBoxShadowHover}`}
                            component={Link}
                            data-id={recipe.id}
                            to={`/recipe/${recipe.id}`}
                        >
                            <div className="relative float-left h-[80px] md:h-[128px] aspect-square mr-2 rounded-l">
                                <div className="absolute rounded-l size-full bg-zinc-100 animate-pulse" />
                                <img className="absolute rounded-l size-full object-cover" src={recipe.image_local ? `/images/${recipe.image_local}` : recipe.image} />
                            </div>

                            <div className="flex items-start justify-between">
                                <Typography className="font-bold text-zinc-700 text-2xl md:text-4xl leading-[2.5rem] pt-0 md:pt-2 md:leading-[3.5rem]" component="div">{recipe.title}</Typography>
                                <div className="flex mt-2">
                                    {userId !== recipe.user_id && <PersonIcon className="text-zinc-300" />}
                                </div>
                            </div>
                        </Paper>
                    ))}
                    {AddFabs}
                </div>
            ) : Recipes.getUserRecipesRequest?.data?.length === 0 ? (
                <div className="container mx-auto px-32 min-h-[calc(100vh-64px)] flex flex-col items-center justify-center text-zinc-300 font-bold text-5xl">
                    Recipes
                    {AddFabs}
                </div>
            ) : /* Loading */ (
                <div className="container mx-auto px-4 md:px-12 lg:px-32 xl:px-48 pt-4 h-[calc(100vh-64px)] overflow-y-hidden">
                    {Array.from({length: Number.parseInt(screen.height / 80, 10)}).map((value, index) => (
                        <div key={`${index + index}`} className="w-full rounded animate-pulse bg-zinc-200 min-h-[96px] md:min-h-[128px] my-4" />
                    ))}
                    {AddFabs}
                </div>
            )}

            <Dialog classes={{paper: 'max-w-none m-0 md:m-8 w-11/12 md:w-9/12 lg:w-5/12'}} maxWidth={false} open={ImportRecipe.modalOpen}>
                <div className="flex items-start justify-between pl-2 pt-1 pb-5">
                    <div className="flex items-center">
                        <AddIcon className="opacity-80 mr-1 text-2xl md:text-3xl" color="secondary" />
                        <div className="font-medium text-gray-600 text-xl md:text-2xl">Add Recipe Via URL</div>
                    </div>
                    <IconButton
                        className={
                            `${'relative -top-1 ml-4 text-zinc-100 brightness-50 hover:brightness-0 focus:brightness-0'} ` +
                            `${(ImportRecipe.getNewRecipeRequest?.error || ImportRecipe.getNewRecipeRequest?.pending) ? 'invisible' : ''}`
                        }

                        onClick={setModalOpenState}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>

                {ImportRecipe.getNewRecipeRequest?.data ? (
                    <>
                        <div className="flex space-x-2 pl-3.5 pr-3">
                            <img className="size-24 rounded" src={ImportRecipe.getNewRecipeRequest.data.image} />
                            <div className="flex-1 flex flex-col justify-between">
                                <div>
                                    <Typography className="font-medium leading-none text-xl md:text-2xl lg:text-3xl" component="div">{ImportRecipe.getNewRecipeRequest.data.title}</Typography>
                                    <Typography className="font-normal text-zinc-500" component="div" variant="subtitle1">{ImportRecipe.getNewRecipeRequest.data.site_name}</Typography>
                                </div>
                                <Typography className="flex items-center justify-between text-zinc-400 text-[.75rem] md:text-base" variant="subtitle2">
                                    <div className="flex items-center"><KitchenIcon className="text-sm md:text-lg mr-0 md:mr-1" /> {ImportRecipe.getNewRecipeRequest.data.ingredients.length} Ingredient{ImportRecipe.getNewRecipeRequest.data.ingredients.length !== 1 && 's'}</div>
                                    <div className="flex items-center"><ListAltIcon className="text-sm md:text-lg mr-0 md:mr-1" /> {ImportRecipe.getNewRecipeRequest.data.instructions_list.length} Step{ImportRecipe.getNewRecipeRequest.data.instructions_list.length !== 1 && 's'}</div>
                                    <div className="flex items-center"><AccessTimeIcon className="text-sm md:text-lg mr-0 md:mr-1" /> {ImportRecipe.getNewRecipeRequest.data.total_time} Minutes</div>
                                </Typography>
                            </div>
                        </div>
                        <div className="flex items-end justify-between pl-2 pr-3 pb-3 pt-8">
                            <Button className="text-zinc-600" color="info" startIcon={<ArrowBackIcon />} onClick={goBackToUrlInput}>Back</Button>
                            {ImportRecipe.addRecipeRequest?.error ? (
                                <Button color="error">An Error Occurred</Button>
                            ) : ImportRecipe.addRecipeRequest?.pending ? (
                                <Button className="pointer-events-none" color="primary" disableElevation endIcon={<CircularProgress className="ml-1" color="inherit" size="1.25rem" />} tabIndex={-1} variant="contained">
                                    Add Recipe
                                </Button>
                            ) : (
                                <Button color="primary" variant="contained" onClick={addNewRecipe}>Add Recipe</Button>
                            )}
                        </div>
                    </>
                ) : (
                    <form className="pl-3.5 pr-3 pt-6 pb-10" onSubmit={submitLookupRecipe}>
                        <TextField
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        {ImportRecipe.getNewRecipeRequest?.error ? (
                                            <IconButton color="error"><ErrorOutlineIcon /></IconButton>
                                        ) : ImportRecipe.getNewRecipeRequest?.pending ? (
                                            <div className="flex items-center justify-center w-10"><CircularProgress size="1.75rem" /></div>
                                        ) : (
                                            <IconButton className={`group ${ImportRecipe.urlInputValue ? '' : 'invisible'}`} color="primary" type="submit">
                                                <CheckIcon className="transition-all group-hover:scale-125 group-focus:scale-125" />
                                            </IconButton>
                                        )}
                                    </InputAdornment>
                                )
                            }}
                            autoFocus
                            fullWidth
                            placeholder="Enter URL"
                            value={ImportRecipe.urlInputValue}

                            onChange={ImportRecipe.getNewRecipeRequest?.pending ? undefined : setUrlInputValue}
                        />
                    </form>
                )}
            </Dialog>
        </>
    );
};

RecipesComponent.propTypes = {
    ImportRecipe: PropTypes.shape({
        addRecipeRequest: PropTypes.shape({data: PropTypes.object, error: PropTypes.object, pending: PropTypes.bool}),
        getNewRecipeRequest: PropTypes.shape({data: PropTypes.object, error: PropTypes.object, pending: PropTypes.bool}),
        modalOpen: PropTypes.bool.isRequired,
        urlInputValue: PropTypes.string.isRequired
    }).isRequired,
    Recipes: PropTypes.shape({
        getUserRecipesRequest: PropTypes.shape({data: PropTypes.array, error: PropTypes.object, pending: PropTypes.bool})
    }).isRequired,
    userId: PropTypes.string.isRequired,

    addNewRecipe: PropTypes.func.isRequired,
    fetchUserRecipes: PropTypes.func.isRequired,
    goBackToUrlInput: PropTypes.func.isRequired,
    setModalOpenState: PropTypes.func.isRequired,
    setUrlInputValue: PropTypes.func.isRequired,
    submitLookupRecipe: PropTypes.func.isRequired
};

export default RecipesComponent;
