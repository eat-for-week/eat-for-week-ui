import PropTypes                                                    from 'prop-types';
import {useEffect}                                                  from 'react';

import AddIcon                                                      from '@mui/icons-material/Add';
import DeleteIcon                                                   from '@mui/icons-material/Delete';
import DeleteOutlineIcon                                            from '@mui/icons-material/DeleteOutline';
import EditIcon                                                     from '@mui/icons-material/Edit';
import SaveIcon                                                     from '@mui/icons-material/Save';
import SickOutlinedIcon                                             from '@mui/icons-material/SickOutlined';

import {Button, Dialog, Divider, IconButton, TextField, Typography} from '@mui/material';


const RecipeComponent = ({
    EditRecipe, Recipes, recipeId, saveValid,

    clearEditSaveRequest, editAdd, editDelete, editSet, fetchUserRecipes, saveEdits, setEditingRecipe
}) => {
    useEffect(() => {
        if (!Recipes.getUserRecipesRequest?.data) {
            fetchUserRecipes();
        }
    }, []);

    const recipe = Recipes.getUserRecipesRequest?.data?.find(userRecipe => userRecipe.id === recipeId);

    return Recipes.getUserRecipesRequest?.error ? (
        <Typography className="container mx-auto px-8 min-h-[calc(100vh-64px)] flex flex-col items-center justify-center font-bold space-y-4" color="error">
            <SickOutlinedIcon className="opacity-90 text-[10rem]" />
            <div className="text-5xl text-center leading-none">Oh no!</div>
            <div className="text-center">There seems to be a problem, please try again later</div>
        </Typography>
    ) : Recipes.getUserRecipesRequest?.pending ? (
        <div className="container mx-auto pt-4 px-4">
            <div className="flex items-end justify-center space-x-4 md:space-x-8 border-0 border-b border-solid border-zinc-100 pb-4">
                <div className="relative size-36 rounded flex-none rounded bg-zinc-300 animate-pulse" />
                <div className="flex-1 h-16 rounded bg-zinc-300 animate-pulse" />
            </div>

            <div className="flex flex-col md:space-x-4 md:flex-row md:items-start mt-4 mb-20 space-y-4 md:space-y-0">
                <div className="flex-0 w-full md:w-1/4 py-4 px-6 space-y-4 rounded border border-solid border-zinc-200">
                    {Array.from({length: 6}).map((_, index) => <div key={`${`${index}`}`} className="h-5 bg-zinc-200 rounded animate-pulse" />)}
                </div>

                <div className="flex-1 px-4 flex-[4] space-y-4">
                    {Array.from({length: 5}).map((_, index) => <div key={`${`${index}`}`} className="h-12 bg-zinc-200 rounded animate-pulse" />)}
                </div>
            </div>
        </div>
    ) : recipe ? (
        <div className="container mx-auto pt-4 px-4">
            {Boolean(EditRecipe.saveEditsRequest?.error) &&
            <Dialog open>
                <Typography className="flex flex-col items-center justify-center px-8 py-4" color="error" component="div">
                    <SickOutlinedIcon className=" opacity-90 text-[10rem]" />
                    <div className="text-5xl text-center leading-none">Oh no!</div>
                    <div className="text-center">Error while saving, please try again later</div>
                    <Button className="mt-8" variant="contained" onClick={clearEditSaveRequest}>Okay</Button>
                </Typography>
            </Dialog>}

            <div className="flex items-end justify-between">
                <div className="flex-0 flex items-end justify-center space-x-4 md:space-x-8">
                    <div className="relative size-24 md:size-36 rounded flex-none">
                        <div className="absolute rounded size-full bg-zinc-100 animate-pulse" />
                        <img className="absolute rounded size-full shadow-xl object-cover" src={recipe.image_local ? `/images/${recipe.image_local}` : recipe.image} />
                    </div>
                    <div className="font-bold text-zinc-700 text-[2.5rem] md:text-[3.5rem] lg:text-[4rem] leading-none">{recipe.title}</div>
                </div>

                <div className="flex-1 flex justify-end space-x-1 md:space-x-4">
                    {EditRecipe.editingSingleRecipeId ? (
                        <>
                            <Button
                                className="text-gray-400 opacity-60 hover:opacity-100 focus:opacity-100 border-current"
                                disabled={Boolean(EditRecipe.saveEditsRequest)}
                                size="small"
                                variant="outlined"

                                onClick={setEditingRecipe}
                            >
                                Cancel
                            </Button>
                            <IconButton color="primary" disabled={!saveValid || Boolean(EditRecipe.saveEditsRequest)} onClick={saveEdits}>
                                <SaveIcon />
                            </IconButton>
                        </>
                    ) : (
                        <IconButton
                            className="transition-all [&:not(:hover):not(:focus):not(:active)]:text-zinc-400 -mr-1"
                            color="primary"
                            data-recipe-id={recipeId}
                            size="large"

                            onClick={setEditingRecipe}
                        >
                            <EditIcon className="text-3xl" />
                        </IconButton>
                    )}
                </div>
            </div>

            <Divider className="border-zinc-100 my-4" />

            <div className="flex flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-4 md:items-start mb-20">
                <div className={`flex-0 w-full md:w-1/4 space-y-3 border-solid border-zinc-200 ${EditRecipe.editingSingleRecipeId ? 'border-0 border-r pr-4' : 'sticky top-20 rounded border py-2 px-4'}`}>
                    <ul className="list-disc space-y-3" role="list">
                        {EditRecipe.ingredients?.map(({localId, text}) => (
                            <li key={localId} className="ml-3 only:list-none">
                                <TextField
                                    key={localId}
                                    InputProps={{
                                        classes: {root: 'items-start px-2 py-1'},
                                        endAdornment: (
                                            <IconButton
                                                className="group opacity-60 hover:opacity-100 focus:opacity-100 ml-2 -mt-1 -mr-2"
                                                color="error"
                                                data-collection-name="ingredients"
                                                data-local-id={localId}
                                                disabled={Boolean(EditRecipe.saveEditsRequest)}
                                                size="small"

                                                onClick={editDelete}
                                            >
                                                <DeleteOutlineIcon className="text-xl group-hover:hidden group-focus:hidden" />
                                                <DeleteIcon className="text-xl hidden group-hover:block group-focus:block" />
                                            </IconButton>
                                        )
                                    }}
                                    className="w-full"
                                    disabled={Boolean(EditRecipe.saveEditsRequest)}
                                    inputProps={{'data-collection-name': 'ingredients', 'data-local-id': localId}}
                                    multiline
                                    value={text}
                                    variant="outlined"

                                    onChange={editSet}
                                />
                            </li>
                        )) || recipe.ingredients.map(text => <li key={text} className="ml-3 only:list-none">{text}</li>)}
                    </ul>
                    {Boolean(EditRecipe.editingSingleRecipeId) &&
                    <div className="flex justify-end">
                        <Button
                            className="text-xs"
                            color="primary"
                            data-collection-name="ingredients"
                            disabled={Boolean(EditRecipe.saveEditsRequest)}
                            size="small"
                            startIcon={<AddIcon />}
                            variant="outlined"

                            onClick={editAdd}
                        >
                            Add Ingredient
                        </Button>
                    </div>}
                </div>

                <div className="flex-1 text-2xl space-y-8">
                    <ol className="list-decimal space-y-8">
                        {EditRecipe.instructions_list?.map(({localId, text}) => (
                            <li key={localId} className="ml-8 only:list-none">
                                <TextField
                                    key={localId}
                                    InputProps={{
                                        classes: {root: 'items-start'},
                                        endAdornment: (
                                            <IconButton
                                                className="group opacity-60 hover:opacity-100 focus:opacity-100 ml-2 -mt-4 -mr-3"
                                                color="error"
                                                data-collection-name="instructions_list"
                                                data-local-id={localId}
                                                disabled={Boolean(EditRecipe.saveEditsRequest)}

                                                onClick={editDelete}
                                            >
                                                <DeleteOutlineIcon className="group-hover:hidden group-focus:hidden" />
                                                <DeleteIcon className="hidden group-hover:block group-focus:block" />
                                            </IconButton>
                                        )
                                    }}
                                    className="w-full"
                                    disabled={Boolean(EditRecipe.saveEditsRequest)}
                                    inputProps={{'data-collection-name': 'instructions_list', 'data-local-id': localId}}
                                    multiline
                                    value={text}
                                    variant="outlined"

                                    onChange={editSet}
                                />
                            </li>
                        )) || recipe.instructions_list.map(instruction => <li key={instruction} className="ml-8 only:list-none">{instruction}</li>)}
                    </ol>
                    {Boolean(EditRecipe.editingSingleRecipeId) &&
                    <div className="flex justify-end">
                        <Button
                            color="primary"
                            data-collection-name="instructions_list"
                            disabled={Boolean(EditRecipe.saveEditsRequest)}
                            startIcon={<AddIcon />}
                            variant="outlined"

                            onClick={editAdd}
                        >
                            Add Step
                        </Button>
                    </div>}
                </div>
            </div>
        </div>
    ) : (
        'Recipe Not Found'
    );
};

RecipeComponent.propTypes = {
    EditRecipe: PropTypes.shape({
        editingSingleRecipeId: PropTypes.string,
        ingredients: PropTypes.arrayOf(PropTypes.shape({localId: PropTypes.number.isRequired, text: PropTypes.string.isRequired})),
        instructions_list: PropTypes.arrayOf(PropTypes.shape({localId: PropTypes.number.isRequired, text: PropTypes.string.isRequired})),
        saveEditsRequest: PropTypes.object
    }).isRequired,
    Recipes: PropTypes.shape({
        getUserRecipesRequest: PropTypes.shape({data: PropTypes.arrayOf(PropTypes.object), error: PropTypes.object, pending: PropTypes.bool})
    }).isRequired,
    recipeId: PropTypes.string.isRequired,
    saveValid: PropTypes.bool.isRequired,

    clearEditSaveRequest: PropTypes.func.isRequired,
    editAdd: PropTypes.func.isRequired,
    editDelete: PropTypes.func.isRequired,
    editSet: PropTypes.func.isRequired,
    fetchUserRecipes: PropTypes.func.isRequired,
    saveEdits: PropTypes.func.isRequired,
    setEditingRecipe: PropTypes.func.isRequired
};

export default RecipeComponent;
